from flask import Flask, render_template, request
import mwparserfromhell
import logging

app = Flask(__name__)

# Set up logging to a file
logging.basicConfig(filename='/data/project/wikitable-autoincrement/www/python/src/logs/error.log', level=logging.DEBUG)

@app.route('/', methods=['GET', 'POST'])
def auto_increment_tool():
    if request.method == 'POST':
        try:
            wikitext_input = request.form['wikitext_input']
            modified_wikitext = add_auto_increment_column(wikitext_input)
            return render_template('result.html', modified_wikitext=modified_wikitext)
        except Exception as e:
            app.logger.error("An error occurred: %s", str(e))
            return render_template('error.html', error_message="Internal Server Error")
    return render_template('index.html')

def add_auto_increment_column(wikitext):
    try:
        wikicode = mwparserfromhell.parse(wikitext)

        # Assuming there is only one table in the Wikitext for simplicity
        table = wikicode.filter_nodes(matches=lambda node: isinstance(node, mwparserfromhell.nodes.table.Table))[0]

        # Add an auto-increment column as the first column
        auto_increment_column = mwparserfromhell.nodes.tag.Tag("td", "auto-increment")
        for row in table.get().contents.nodes:
            row.insert(0, auto_increment_column.clone())

        return str(wikicode)
    except Exception as e:
        app.logger.error("An error occurred in add_auto_increment_column: %s", str(e))
        raise

if __name__ == '__main__':
    app.run(debug=True)
